import { Pro8samplePage } from './app.po';

describe('pro8sample App', () => {
  let page: Pro8samplePage;

  beforeEach(() => {
    page = new Pro8samplePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
