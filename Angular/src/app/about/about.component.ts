import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
aboutContent:string;
  constructor() { }

  ngOnInit() {
  this.aboutContent= "test New Home Source Professional, from Builders Digital Experience LLC, is the premier web site for real estate professionals to get all of the information that they need about new homes for their clients.Featuring thousands of new homes, communities and builders, New Home Source Professional is the most comprehensive resource available to agents -- and it is free to use.";
  }

}
