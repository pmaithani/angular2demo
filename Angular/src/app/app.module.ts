import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routes';
import { AppComponent } from './app.component';
import { HomecontrolleComponent } from './homecontrolle/homecontrolle.component';


import { TestingComponent } from './testing/testing.component';
import { SearchComponentComponent } from './search-component/search-component.component';
import { HttpService } from 'app/http-service';
import { LoggingService } from 'app/logging.service';
import { HomeService } from 'app/home/home.service';
import { AboutComponent } from './about/about.component';
import { ErrorComponent } from './error/error.component';
import { HelpComponent } from './help/help.component';
import { SignUpComponent } from './authetication/signup/signup.component';
import { SigninComponent } from './authetication/signin/signin.component';
import { AuthService } from "app/authetication/auth.service";
import { AuthGuard } from "app/authetication/auth.guard.service";
import { HomeModule } from "app/home/home.module";
@NgModule({
  declarations: [
    AppComponent,

    TestingComponent,

    AboutComponent,
    ErrorComponent,
    HelpComponent,
    SignUpComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    HomeModule
  ],
  providers: [HttpService,LoggingService,HomeService,AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
