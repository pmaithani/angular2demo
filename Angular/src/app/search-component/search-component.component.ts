import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { HomeService } from 'app/home/home.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.css']
})
export class SearchComponentComponent implements OnInit {

 @ViewChild('f') signupForm: NgForm;

  constructor(private homeservice:HomeService) { }

  ngOnInit() {
  }
GetMarket()
 {
console.log(this.signupForm);
  this.homeservice.userSearch.next({marketId: this.signupForm.value.marketInput, stateId : this.signupForm.value.StateInput.value});


}
}
