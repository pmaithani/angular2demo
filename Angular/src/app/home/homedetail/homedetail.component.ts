import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';
import { HomeService } from "app/home/home.service";
import { APIHome } from "app/home/homeapi";

@Component({
  selector: 'app-homedetail',
  templateUrl: './homedetail.component.html',
  styleUrls: ['./homedetail.component.css'],
  providers:[HomeService]
})
export class HomedetailComponent implements OnInit {

  constructor(private homeService :HomeService,private route: ActivatedRoute) { }
    private sub: Subscription;
public input:APIHome=new APIHome();
  ngOnInit() {

     this.sub = this.route.params.subscribe(
            params => {
                let id = +params['id'];
                this.homeDetail(id.toString());
        });
  }

   GetStatus(input:APIHome)
  {

 switch (input.Status)
            {
                case "A":
                    input.Status = "Available Now";
                    break;
                case "M":
                    input.Status = "Model Home";
                    break;
                case "Q":
                    input.Status = "Quick Move-In";
                    break;
                case "R":
                    input.Status = "Ready to Build";
                    break;
                case "UC":
                    input.Status = "Under Construction";
                    break;
                case "U":
                    input.Status = "Under Construction";
                    break;
            }
            return input.Status;

  }
homeDetail(id:string)
  {

    console.log(id);
    this.homeService.getHomeDetail(id.toString()).subscribe(data=>
    {
      this.input=data;
      console.log(this.input);
      });


  }
}
