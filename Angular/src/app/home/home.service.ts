import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { HomesList } from "app/home/homeInput";
import { APIHome } from "app/home/homeapi";
import { HttpService } from "app/http-service";
import { LoggingService } from "app/logging.service";



@Injectable()
export class HomeService {

   constructor(private http: Http,private common: HttpService,private logging:LoggingService) { }
 // tslint:disable-next-line:member-ordering
    userSearch = new Subject();

getHomeDetail(id: string): Observable<APIHome> {

        let url: string = 'http://localhost:58669/api/newhomesource/HomeDetail?id=' + id;

        var data=this.common.getdData(url).map(r=>
        {
            this.logData(url,r.status);
        return r.json() as APIHome

        }).do(data =>{})
            .catch(this.handleErrors);
        return data;


    }



GetAll(maketId:number,state:string):Observable<HomesList[]>
{
 let url:string='http://localhost:58669/api/newhomesource/homesResult?marektId='+maketId+'&state='+state;

return this.common.getdData(url)
            .map(response => {
       this.logData(url,response.status);
          return    response.json() as HomesList[]

          })
            .do(data =>{})
            .catch(this.handleErrors);
// return this.http.get(url).map(this.extractData);

}

private logData(url:string,status:number)
{
this.logging.log(url,status);

}
private handleErrors(error: Response) {
        console.log(error);
        return Observable.throw(error.json().message || 'Error while trying to fetch data');
    }
private extractData(response: Response) {
        let body = response.json();
        let data = body as HomesList[];
        return data || {};
    }

}
