import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeListComponent } from 'app/home/home-list/home-list.component';
import { HomedetailComponent } from 'app/home/homedetail/homedetail.component';
import { SearchComponentComponent } from "app/search-component/search-component.component";
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from "@angular/forms";



@NgModule({
  declarations: [
    HomeListComponent,
    HomedetailComponent,
    SearchComponentComponent,
  ],
  imports: [
    CommonModule,
RouterModule,
FormsModule
  ]
})
export class HomeModule {}
