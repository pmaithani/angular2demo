import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from "app/home/home.service";
import { HomesList } from "app/home/homeInput";
@Component({
  selector: 'app-home-list',
  templateUrl: './home-list.component.html',
  styleUrls: ['./home-list.component.css'],

})
export class HomeListComponent implements OnInit {
 public homes:HomesList[];
 public marektId:number;
 public stateId:string;
  constructor(private homeService :HomeService,private router: Router) { }
isShow=false;
  ngOnInit() {

 this.homeService.userSearch.subscribe(
      (data: any) => {

        this.homeService.GetAll(data.marketId, data.stateId).subscribe(grametric => {
                        this.homes=grametric;
                        this.isShow=true;
                        console.log("good");
                        });
        }
      );


  }

  GetStatus(input:HomesList)
  {

 switch (input.Status)
            {
                case "A":
                    input.Status = "Available Now";
                    break;
                case "M":
                    input.Status = "Model Home";
                    break;
                case "Q":
                    input.Status = "Quick Move-In";
                    break;
                case "R":
                    input.Status = "Ready to Build";
                    break;
                case "UC":
                    input.Status = "Under Construction";
                    break;
                case "U":
                    input.Status = "Under Construction";
                    break;
            }
            return input.Status;

  }
  GetAll(marketId:number,stateId:string):void
  {
//       debugger;
// this.homeService.GetAll().subscribe(home => {
//                         this.homes=home;
//                         console.log(this.homes);
// });
}
}
