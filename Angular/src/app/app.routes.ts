"use strict";
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomecontrolleComponent } from './homecontrolle/homecontrolle.component';
import { HomeListComponent } from "app/home/home-list/home-list.component";
import { HomedetailComponent } from "app/home/homedetail/homedetail.component";
import { ErrorComponent } from "app/error/error.component";
import { AboutComponent } from "app/about/about.component";
import { HelpComponent } from "app/help/help.component";
import { SigninComponent } from "app/authetication/signin/signin.component";
import { SignUpComponent } from "app/authetication/signup/signup.component";
import { AuthGuard } from "app/authetication/auth.guard.service";
var router_1 = require("@angular/router");

var appRoutes = [
 { path: '', redirectTo: 'homes', pathMatch: 'full' },
    { path: 'homes', component: HomeListComponent,canActivate: [AuthGuard]},
     { path: 'home/detail/:id', component: HomedetailComponent,canActivate: [AuthGuard]},
//   { path: 'homes', component: HomeListComponent,canActivate: [AuthGuard]},
   { path: 'about', component: AboutComponent,canActivate: [AuthGuard]},
   { path: 'Help', component: HelpComponent,canActivate: [AuthGuard]},
  { path: 'not-found', component: ErrorComponent, data: {message: 'Page not found!'} },
   { path: 'signup', component: SignUpComponent },
  { path: 'signin', component: SigninComponent },

  { path: '**', redirectTo: '/not-found' }
];
export const routing: ModuleWithProviders =
    RouterModule.forRoot(appRoutes);
