import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { HttpService } from "app/http-service";
import { LoggingService } from "app/logging.service";
import { Login } from './login';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthService {
  token: string;
  username:string;
  constructor(private http: Http,private common: HttpService,private logging:LoggingService,private router: Router) { }


signupUser(email: string, password: string) : Observable<Login> {

        let url: string = 'http://localhost:58669/api/newhomesource/Login?email=' +email+'&password='+password;

        var data=this.common.getdData(url).map(r=>
        {
            this.logData(url,r.status);
        return r.json() as Login

        }).do(data =>{})
            .catch(this.handleErrors);
        return data;


    }

signinUser(email: string, password: string) : Observable<Login> {
debugger;
        let url: string = 'http://localhost:58669/api/newhomesource/Login?email=' +email+'&password='+password;

    return    this.common.getdData(url).map(r=>
        {
            this.logData(url,r.status);
        var logindata= r.json() as Login

        if(logindata.IsAuthenticated){
          debugger;
          this.token=logindata.Token;
          this.username=logindata.Email;
           this.router.navigate(['/']);
        }else
        {
          console.log("User not Authenticated");
        }
return logindata;
        }).do(data =>{})
            .catch(this.handleErrors);



    }

  logout() {

    this.token = null;
  }

  getToken() {

    return this.token;
  }
getEmail()
{
return this.username;

}
  isAuthenticated() {
    debugger;
    return this.token != null;
  }

private logData(url:string,status:number)
{
this.logging.log(url,status);

}
private handleErrors(error: Response) {
        console.log(error);
        return Observable.throw(error.json().message || 'Error while trying to fetch data');
    }

}
