import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApplicationSettings {
    baseUrl = '';
    serviceBaseUrl = '';
    appPage = '';

    constructor(public http: Http) { 
    }

    public getUrl() {
            return new Promise((resolve, reject) => {
              this.http.get('config.json')  // path of config.json file
                .map(res => res.json())
                .subscribe(
                  (data: any) => {
                    this.baseUrl = data.baseUrl;
                    this.serviceBaseUrl = data.serviceBaseUrl;
                    resolve(true);
                  },
                  err => console.log(err)
                );
            });
          }

    retrieveUrl(): string {
        return this.baseUrl;
    }

      retrieveServiceUrl(): string {
        return this.serviceBaseUrl;
    }
}