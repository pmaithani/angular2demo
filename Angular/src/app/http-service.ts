import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';



@Injectable()
export class HttpService {

   constructor(private http: Http) { }


getdData(url: string):Observable<Response>
{

      // tslint:disable-next-line:no-unused-expression
      let ob = this.http.get(url);
      return ob;
    }



}
