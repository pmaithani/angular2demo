import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  helpContent: string;

  constructor() { }

  ngOnInit() {
 this.helpContent="Site HelpBelow you’ll find answers to some standard questions from agents using the site. If you can’t find the answer that you are looking for, feel free to contact us and we will be happy to help you out.What are the benefits of having an account?New Home Source Professional is a free service designed to complement the MLS and help agents find new home inventory, share it with clients, communicate with builders, view compensation information and read educational information about selling new homes. Signing up for an account will allow you to:Use your personal ShowingNew.com site to send listings with clients and drive prospect leads from your website. Learn more about your free ShowingNew.com site.View information not available to buyers, such as promotions and events for agents, compensation details, builder policy documents and buyer registration forms.Communicate directly with builders.Do builders ever get my contact information?Yes. We want to be fully transparent about this. When you email a builder, send a saved listing with a client or view a Client Version of a listing report; or a client saves a listing or views a listing report on their own, your contact information is shared so that the builder can follow up with you and offer assistance. In addition to helping you sell more new homes, one of the primary goals of New Home Source Professional is to develop long-lasting, mutually beneficial relationships between you and builders in your area.Do builders ever get information about my clients?No. We never EVER share your client information with builders. We absolutely want to preserve the relationships that you have with your clients, and New Home Source Professional is designed to be a safe environment to conduct new home research and share details with your clients. Builders will never see client information.Is compensation guaranteed?Still need help";

  }

}
