﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Angular2Demo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{*anything}",
            //    defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            //);
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                // Set a constraint to only use this for routes identified as server-side routes
                constraints: new
                {
                    serverRoute = new ServerRouteConstraint(url =>
                    {
                        return url.PathAndQuery.StartsWith("/Home",
                            StringComparison.InvariantCultureIgnoreCase);
                    })
                });



          

            routes.MapRoute(
                name: "angular1",
                url: "employee/{id}",
                defaults: new { controller = "Home", action = "employee", id = UrlParameter.Optional }, // The view that bootstraps Angular 2
                constraints: new
                {
                    serverRoute = new ServerRouteConstraint(url =>
                    {
                        return url.PathAndQuery.StartsWith("/employee",
                            StringComparison.InvariantCultureIgnoreCase);
                    })
                });

            routes.MapRoute(
                name: "angular",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index" } // The view that bootstraps Angular 2
            );

        }
    }
}

