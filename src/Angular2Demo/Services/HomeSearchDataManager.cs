﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Angular2Demo.Data;
using Angular2Demo.Helpers;
using Angular2Demo.Models;

namespace Angular2Demo.Services
{
    public class HomeSearchDataManager
    {
        public static List<HomeSearchResult> PerformSearch(int marketid, string state)
        {
            HomeSearchResults searchResult = null;
            var siteUrl = "http://sprint-api.newhomesource.com/api/V2/search/homes?noboyl=1&client=NhsPro&partnerid=88&pagesize=24&state=" +state + "&marketid=" + marketid + "&sortby=Random&sessiontoken=ProSessionToken&page=1";
                String jsonResponse = WebDataManager.GetWebResponse(siteUrl);
                ApiResponse apiResponseInstance = SerializationHelper.DeserializeContent<ApiResponse>(jsonResponse);
            if (!string.IsNullOrEmpty(jsonResponse))
            {
                if (APIStatusModel.IsApiResponceStatusTrue(apiResponseInstance, siteUrl))
                {
                    searchResult = SerializationHelper.DeserializeContent<HomeSearchResults>(jsonResponse);
   
                }
            }
        
            return searchResult.Results;
        }

    }
}
