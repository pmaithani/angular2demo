﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Angular2Demo.Data;
using Angular2Demo.Helpers;
using Angular2Demo.Models;

namespace Angular2Demo.Services
{
    public class ListingDataManager
    {
        public static ListingDetails GetListingDetails(int partnerID, string listingID)
    {
    string url = GetListingUrl(partnerID,listingID);
    ListingDetails details = null;
    //ApiVideos videosData = null;
    string webResponse = WebDataManager.GetWebResponse(url);
    ApiResponse apiResponseInstance = SerializationHelper.DeserializeContent<ApiResponse>(webResponse);
        if (!string.IsNullOrEmpty(webResponse))
        {

                details = SerializationHelper.DeserializeContent<ListingDetails>(webResponse);
            
        }
           

    return details;
}
        private static string GetListingUrl(int partnerId, string listingId)
        {
            string url = string.Format("http://sprint-api.newhomesource.com/api/V2/detail/home?excludeevents=1&page=1&pagesize=24&sortby=Random&excludevideotour=1&excludeimages=1&excludefloorplans=1&client=NhsPro&excludenonpdfbrochure=1&excludeenvisionurl=1&excludevideos=1&sessiontoken=ProSessionToken&excludeinteractivemedia=1&partnerid=88&excludehomeoptions=1&exlucedefloorplanviewerurl=1&excludepromotions=1&noboyl=1&excludetollfreenumber=1&excludecustomamenities=1&planid="+ listingId);
            return url;
        }


    }
}