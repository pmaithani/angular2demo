﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Angular2Demo.Data
{
    public class WebDataManager
    {
        public static string GetWebResponse(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            string responseJson;
            using (var streamReader = new StreamReader(res.GetResponseStream()))
            {
                responseJson = streamReader.ReadToEnd();
            }
            return responseJson;

        }
    }
}