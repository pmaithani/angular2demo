﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Angular2Demo.Helpers
{
    public class SerializationHelper
    {
        public static T DeserializeContent<T>(string json)
        {
            using (var memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                return (T)serializer.ReadObject(memoryStream);

            }
        }

        public static string JsonSerializer(object data)
        {
            if (data != null)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(data.GetType());
                string serializedJson = string.Empty;
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    serializer.WriteObject(memoryStream, data);
                    serializedJson = Encoding.Default.GetString(memoryStream.ToArray());
                }
                return serializedJson;
            }
            else
            {
                return string.Empty;
            }
        }

    }
}