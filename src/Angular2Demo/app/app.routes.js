"use strict";
var router_1 = require("@angular/router");
var employees_component_1 = require("./employees/employees.component");
var about_1 = require("./employees/about");
var home_component_1 = require("./employees/home.component");
var employee_detail_1 = require("./employees/employee.detail");
var appRoutes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: 'employees', component: employees_component_1.EmployeesComponent },
    { path: 'employee/:id', component: employee_detail_1.EmployeeDetailsComponent },
    { path: 'about', component: about_1.AboutComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//import { ModuleWithProviders } from '@angular/core';
//import { Routes, RouterModule } from '@angular/router';
//import { EmployeesComponent } from './employees/employees.component'
//import { EmployeeDetailsComponent } from './employees/employee.detail';
//import { employeeRoutes } from './routes/employee.route';
//// Route Configuration
//export const routes: Routes = [
//    {
//        path: '',
//        redirectTo: '',
//        pathMatch: 'full'
//    },
//    { path: 'employees', component: EmployeesComponent },
//    { path: 'employee/:id', component: EmployeeDetailsComponent }
//    // Add employee routes form a different file
//    //...employeeRoutes
//];
//// Deprecated provide
//// export const APP_ROUTER_PROVIDERS = [
////   provideRouter(routes)
//// ];
//export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
//# sourceMappingURL=app.routes.js.map