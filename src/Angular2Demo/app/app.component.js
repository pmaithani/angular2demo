"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var AppComponent = (function () {
    function AppComponent(route) {
        this.route = route;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id']; // (+) converts string 'id' to a number
            // In a real app: dispatch action to load the details here.
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "my-app",
        templateUrl: '/app/app.component.html'
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute])
], AppComponent);
exports.AppComponent = AppComponent;
//import { Component } from '@angular/core';
//@Component({
//    moduleId: module.id,
//    selector: 'my-app',
//    templateUrl: '/app/app.component.html'
//})
// //App Component class
//export class AppComponent { }
//import { Component } from '@angular/core';
//@Component({
//    moduleId: module.id,
//    selector: 'my-app',
//    template: ` <nav>
//            <a routerLink="/department">Department</a> | 
//            <a routerLink="/about">About</a>            
//        </nav>
//        <router-outlet></router-outlet>`
//})
//export class AppComponent { } 
//# sourceMappingURL=app.component.js.map