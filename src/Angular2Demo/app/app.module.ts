import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { HttpModule } from '@angular/http';
import { routing } from './app.routes';
import { EmployeesComponent } from './employees/employees.component';
import { AboutComponent } from './employees/about';
import { EmployeeDetailsComponent } from './employees/employee.detail';
import { HomeComponent } from './employees/home.component';
import { EmployeeService } from './employees/employee.service';


@NgModule({
    imports: [BrowserModule, ReactiveFormsModule, HttpModule, routing, Ng2Bs3ModalModule],
    declarations: [AppComponent, EmployeesComponent, EmployeeDetailsComponent, HomeComponent, AboutComponent],
    providers: [{ provide: APP_BASE_HREF, useValue: '/' }, EmployeeService],
    bootstrap: [AppComponent]
})
export class AppModule {
 
}

































//import { NgModule } from '@angular/core';
//import { BrowserModule } from '@angular/platform-browser';
//import { HashLocationStrategy, LocationStrategy } from '@angular/common';
//import { HttpModule } from '@angular/http';
//import { AppComponent } from './app.component';
//import { RouterModule } from '@angular/router';
//import { EmployeesComponent } from './employees/employees.component';
//import { EmployeeDetailsComponent} from './employees/employee.detail';
//import { EmployeeService } from './employees/employee.service';
//import { routing } from './app.routes';


//@NgModule({

//    imports: [BrowserModule, RouterModule, HttpModule, routing],
//    declarations: [AppComponent, EmployeesComponent, EmployeeDetailsComponent],
//    providers: [EmployeeService],
//    bootstrap: [AppComponent]
//})

//export class AppModule { }


























//import { NgModule } from '@angular/core';
//import { BrowserModule } from '@angular/platform-browser';
//import { RouterModule, Routes } from '@angular/router';

//import { AppComponent } from './app.component';
//import { DepartmentComponent } from './routingexample/department.component';
//import { AboutComponent } from './routingexample/about.component';
//const routes: Routes = [
//    { path: '', redirectTo: '/department', pathMatch: 'full' },
//    { path: 'department', component: DepartmentComponent },
//    { path: 'about', component: AboutComponent }
//];
//@NgModule({
//    bootstrap: [AppComponent],
//    imports: [
//        BrowserModule,
//        RouterModule.forRoot(routes)
//    ],
//    declarations: [
//        AppComponent,
//        DepartmentComponent,
//        AboutComponent
//    ]
//})
//export class AppModule { }
