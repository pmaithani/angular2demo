import { Component, OnInit } from "@angular/core"
import { ActivatedRoute } from '@angular/router';

@
Component({
    selector: "my-app",
     templateUrl: '/app/app.component.html'
    //styleUrls: ['app/app.component.css']
})

export class AppComponent implements OnInit {
    id: number;
    private sub: any;
    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id']; // (+) converts string 'id' to a number

            // In a real app: dispatch action to load the details here.
        });
    }

}



//import { Component } from '@angular/core';

//@Component({
//    moduleId: module.id,
//    selector: 'my-app',
//    templateUrl: '/app/app.component.html'
//})
// //App Component class
//export class AppComponent { }

































//import { Component } from '@angular/core';

//@Component({
//    moduleId: module.id,
//    selector: 'my-app',
//    template: ` <nav>
//            <a routerLink="/department">Department</a> | 
//            <a routerLink="/about">About</a>            
//        </nav>
//        <router-outlet></router-outlet>`
//})
//export class AppComponent { }