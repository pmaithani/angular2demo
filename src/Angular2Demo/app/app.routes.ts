﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees/employees.component'
import { AboutComponent } from './employees/about'
import { HomeComponent } from './employees/home.component';
import { EmployeeDetailsComponent } from './employees/employee.detail';



const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'employees', component: EmployeesComponent },
    { path: 'employee/:id', component: EmployeeDetailsComponent},
    { path: 'about', component: AboutComponent }
];

export const routing: ModuleWithProviders =
    RouterModule.forRoot(appRoutes);































//import { ModuleWithProviders } from '@angular/core';
//import { Routes, RouterModule } from '@angular/router';
//import { EmployeesComponent } from './employees/employees.component'
//import { EmployeeDetailsComponent } from './employees/employee.detail';
//import { employeeRoutes } from './routes/employee.route';
//// Route Configuration
//export const routes: Routes = [
//    {
//        path: '',
//        redirectTo: '',
//        pathMatch: 'full'
//    },
//    { path: 'employees', component: EmployeesComponent },
//    { path: 'employee/:id', component: EmployeeDetailsComponent }
//    // Add employee routes form a different file
//    //...employeeRoutes
//];

//// Deprecated provide
//// export const APP_ROUTER_PROVIDERS = [
////   provideRouter(routes)
//// ];

//export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
