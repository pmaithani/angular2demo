"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var app_component_1 = require("./app.component");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var http_1 = require("@angular/http");
var app_routes_1 = require("./app.routes");
var employees_component_1 = require("./employees/employees.component");
var about_1 = require("./employees/about");
var employee_detail_1 = require("./employees/employee.detail");
var home_component_1 = require("./employees/home.component");
var employee_service_1 = require("./employees/employee.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.ReactiveFormsModule, http_1.HttpModule, app_routes_1.routing, ng2_bs3_modal_1.Ng2Bs3ModalModule],
        declarations: [app_component_1.AppComponent, employees_component_1.EmployeesComponent, employee_detail_1.EmployeeDetailsComponent, home_component_1.HomeComponent, about_1.AboutComponent],
        providers: [{ provide: common_1.APP_BASE_HREF, useValue: '/' }, employee_service_1.EmployeeService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//import { NgModule } from '@angular/core';
//import { BrowserModule } from '@angular/platform-browser';
//import { HashLocationStrategy, LocationStrategy } from '@angular/common';
//import { HttpModule } from '@angular/http';
//import { AppComponent } from './app.component';
//import { RouterModule } from '@angular/router';
//import { EmployeesComponent } from './employees/employees.component';
//import { EmployeeDetailsComponent} from './employees/employee.detail';
//import { EmployeeService } from './employees/employee.service';
//import { routing } from './app.routes';
//@NgModule({
//    imports: [BrowserModule, RouterModule, HttpModule, routing],
//    declarations: [AppComponent, EmployeesComponent, EmployeeDetailsComponent],
//    providers: [EmployeeService],
//    bootstrap: [AppComponent]
//})
//export class AppModule { }
//import { NgModule } from '@angular/core';
//import { BrowserModule } from '@angular/platform-browser';
//import { RouterModule, Routes } from '@angular/router';
//import { AppComponent } from './app.component';
//import { DepartmentComponent } from './routingexample/department.component';
//import { AboutComponent } from './routingexample/about.component';
//const routes: Routes = [
//    { path: '', redirectTo: '/department', pathMatch: 'full' },
//    { path: 'department', component: DepartmentComponent },
//    { path: 'about', component: AboutComponent }
//];
//@NgModule({
//    bootstrap: [AppComponent],
//    imports: [
//        BrowserModule,
//        RouterModule.forRoot(routes)
//    ],
//    declarations: [
//        AppComponent,
//        DepartmentComponent,
//        AboutComponent
//    ]
//})
//export class AppModule { }
//# sourceMappingURL=app.module.js.map