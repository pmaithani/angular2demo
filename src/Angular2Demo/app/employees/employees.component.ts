﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from './employee.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { IEmployee } from './employee';
import { DBOperation } from '../Shared/enum';
import { Global } from '../Shared/global';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
    templateUrl: '/app/employees/htmltemplates/app.employees.html'
    //styleUrls: ['app/app.component.css']
})

export class EmployeesComponent implements OnInit {

    @ViewChild('modal') modal: ModalComponent;
    employees: IEmployee[];
    employee: IEmployee;
    msg: string;
    indLoading: boolean = false;
    employeeFrm: FormGroup;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;
    private sub: any;
    id: any;

    constructor(private fb: FormBuilder, private router: Router, private employeeService: EmployeeService, private route: ActivatedRoute) { }

    ngOnInit(): void {
     
            this.sub = this.route.params.subscribe(params => {
            this.id = params['id'];

    });
        this.employeeFrm = this.fb.group({
            Id: [''],
            Name: ['', Validators.required],
            Gender: ['', Validators.required],
            Designation: [''],
            Email: [''],
            Age: [''],
            Phone:['']
        });
        this.loadEmployees();
    }

    loadEmployees(): void {
        this.indLoading = true;
        this.employeeService.get(Global.BASE_Employee_ENDPOINT)
            .subscribe(employees => {
                     this.employees = employees; this.indLoading = false;
                },
                error => this.msg = <any>error);
    }


    addEmployee() {
        this.dbops = DBOperation.create;
        this.SetControlsState(true);
        this.modalTitle = "Add New employee";
        this.modalBtnTitle = "Add";
        this.employeeFrm.reset();
        this.modal.open();
    }

    editEmployee(id: number) {
        this.dbops = DBOperation.update;
        this.SetControlsState(true);
        this.modalTitle = "Edit User";
        this.modalBtnTitle = "Update";
        this.employee = this.employees.filter(x => x.Id === id)[0];
        this.employeeFrm.setValue(this.employee);
        this.modal.open();
    }

    deleteEmployee(id: number) {
        this.dbops = DBOperation.delete;
        this.SetControlsState(false);
        this.modalTitle = "Confirm to Delete?";
        this.modalBtnTitle = "Delete";
        this.employee = this.employees.filter(x => x.Id === id)[0];
        this.employeeFrm.setValue(this.employee);
        this.modal.open();
    }

    onSubmit(formData: any) {
        this.msg = "";

        switch (this.dbops) {
        case DBOperation.create:
                this.employeeService.post(Global.BASE_Employee_ENDPOINT, formData._value).subscribe(
                data => {
                    if (data == 1) //Success
                    {
                        this.msg = "Data successfully added.";
                        this.loadEmployees();
                    }
                    else {
                        this.msg = "There is some issue in saving records, please contact to system administrator!"
                    }

                    this.modal.dismiss();
                },
                error => {
                    this.msg = error;
                }
            );
            break;
        case DBOperation.update:
                this.employeeService.put(Global.BASE_Employee_ENDPOINT, formData._value.Id, formData._value).subscribe(
                data => {
                    if (data == 1) //Success
                    {
                        this.msg = "Data successfully updated.";
                        this.loadEmployees();
                    }
                    else {
                        this.msg = "There is some issue in saving records, please contact to system administrator!"
                    }

                    this.modal.dismiss();
                },
                error => {
                    this.msg = error;
                }
            );
            break;
        case DBOperation.delete:
                this.employeeService.delete(Global.BASE_Employee_ENDPOINT, formData._value.Id).subscribe(
                data => {
                    if (data == 1) //Success
                    {
                        this.msg = "Data successfully deleted.";
                        this.loadEmployees();
                    }
                    else {
                        this.msg = "There is some issue in saving records, please contact to system administrator!"
                    }

                    this.modal.dismiss();
                },
                error => {
                    this.msg = error;
                }
            );
            break;

        }
    }
    getDetail(employee:IEmployee) {
        this.router.navigate(['/employee', employee.Id]);
}

    SetControlsState(isEnable: boolean) {
        isEnable ? this.employeeFrm.enable() : this.employeeFrm.disable();
    }
}
















































//import { Component, OnInit } from '@angular/core';
//import { EmployeeService } from './employee.service';
//import 'rxjs/add/operator/toPromise';
//import { Observable } from 'rxjs/observable';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';

//@Component({
//    selector: 'my-app',
//    templateUrl: '/app/employees/htmltemplates/app.employees.html',
//    styleUrls: ['app/app.component.css']
//})

//export class EmployeesComponent implements OnInit {
//   // employees: Observable<Object[]>;
//    employees: any = {};
//    constructor(private employeeService: EmployeeService) {
//        this.employees =
//         this.employeeService.findEmployees();
//    }
//    ngOnInit() {

//      //  this.employees = this.employeeService.findEmployees();

//    }
  
//    //ngOnInit() {
//    //    this.employees = ((this.employeeService.findEmployees()) as any);
//    //}

//}
