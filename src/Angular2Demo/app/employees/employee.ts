﻿export interface IEmployee {
    Id: number,
    Name: string,
    Designation: string,
    Age: number,
    Email: string,
    Phone: string,
    Gender: string
}