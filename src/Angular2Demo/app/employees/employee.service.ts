﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';

@Injectable()
export class EmployeeService {
    constructor(private http: Http) { }

    get(url: string): Observable<any> {
        console.log(url + "Employee");
        return this.http.get(url+"Employee")
            .map((response: Response) =>
                <any>response.json())
            .catch(this.handleError);
    }

    post(url: string, model: any): Observable<any> {
        let body = JSON.stringify(model);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url + "Post", body, options)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    put(url: string, id: number, model: any): Observable<any> {
        let body = JSON.stringify(model);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put(url+"Put" + id, body, options)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    delete(url: string, id: number): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(url+"Delete" + id, options)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }
    detail(url: string, id: number): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        console.log(url+"Detail");
        return this.http.get("http://localhost:58669/api/employeeapi/" + "Detail?id="+id)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}

















































//// Imports
//import { Injectable } from '@angular/core';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
//import { Observable } from 'rxjs/observable'
//import { Http } from '@angular/http';
//import { Jsonp, URLSearchParams } from '@angular/http';
//@Injectable()
//export class EmployeeService { 
//    employees: Observable<Employee[]>;
//    employee:Observable<Employee>;

//    private employeeDetailUrl ='/Home/GetEmployeeDetailById';
//    constructor(private http: Http) { }
//    findEmployees():any{
//        let employeesUrl = '/Home/GetEmploeesSummary';
//          ((this.http
//            .get(employeesUrl).
//            subscribe(response => {
//                let data = response.text() ? response.json() : [{}];
//                if (data) {
//                    this.employees = data;

//                }
//                return JSON.stringify(this.employees);
//            }, error => console.log("did not get any responce!"))));
//        return this.employees;
//    }
//    findEmployeeById(id: number) {
//            let params = new URLSearchParams();
//            params.set('id', (id) as any);
//            params.set('format', 'json');
//            params.set('callback', 'JSONP_CALLBACK');
//            this.http
//                .get(this.employeeDetailUrl, { search: params }).toPromise()
//                .then(response => this.employee= response.json());
//        return this.employee;
//    }
//}
//export class Employee {
//    Name: number;
//    Id: string;
//    Designation: string;
//    Age :number; 
//    Email:string;
//    Phone:string; 
//}
//export class QueryStringParameters {
//    public Id: number = null;
//}












    //constructor(private jsonp: Jsonp) { }

    //private employeesUrl = '/Home/AjaxCall';

    //findEmployees() {
    //    return this.jsonp
    //        .get(this.employeesUrl)
    //        .map(response => <string[]>response.json());
    //}

    //// get a pet based on their id
    //findEmployeeById(id: string) {
    //    let params = new URLSearchParams();
    //    params.set('id', id);
    //    params.set('format', 'json');
    //    params.set('callback', 'JSONP_CALLBACK');

    //    // Return response
    //    return this.jsonp
    //        .get(this.employeesUrl, { search: params })
    //        .map(response => <string[]>response.json().petfinder.pet);
    //}
//}