﻿
import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from './employee.service'
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { IEmployee } from './employee';
import { Global } from '../Shared/global';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'my-app',
    templateUrl: '/app/employees/htmltemplates/app.employee-detail.html'
    //styleUrls: ['/app/app.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
 
    employee: IEmployee;
    employees: IEmployee[];
    msg: string;
    private sub: any;
    id:any;

    constructor(private employeeService: EmployeeService, private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.sub = this.route.params.subscribe(params => {
             this.id = params['id'];
          
        });
        this.loadEmployee(this.id);
       
    }

    loadEmployee(id: number): void {
 
        this.employeeService.detail(Global.BASE_Employee_ENDPOINT, id)
            .subscribe(employee => {
                this.employees = employee;
    
                },
                error => this.msg = <any>error);
    }
}



// ====== ./app/Dogs/dog-details.component ======
// Imports
//import { Component, OnInit } from '@angular/core';
//import { EmployeeService } from '../employees/employee.service';
//import { Observable } from 'rxjs/Observable';
//import { ActivatedRoute } from '@angular/router';

//@
//Component({
//    selector: 'my-app',
//    templateUrl: '/app/employees/htmltemplates/app.employee-detail.html',
//    styleUrls: ['app/app.component.css']
//})

////Component class implementing OnInit
//export class EmployeeDetailsComponent implements OnInit {
//    // Private properties for binding
//    private sub: any;
//    private employee: Observable<Object>;
//    constructor(private employeeService: EmployeeService, private route: ActivatedRoute) {

//    }

//    // Load data ones componet is ready
//    ngOnInit() {
//        // Subscribe to route params
//        this.sub = this.route.params.subscribe(params => {
//            let id = params['id'];
//            // Retrieve employee with Id route param
//         this.employee= this.employeeService.findEmployeeById(id);
//        });
//    }

//    ngOnDestroy() {
//        // Clean sub to avoid memory leak
//        this.sub.unsubscribe();
//    }
//}