"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var employee_service_1 = require("./employee.service");
var global_1 = require("../Shared/global");
var router_1 = require("@angular/router");
var EmployeeDetailsComponent = (function () {
    function EmployeeDetailsComponent(employeeService, route) {
        this.employeeService = employeeService;
        this.route = route;
    }
    EmployeeDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
        this.loadEmployee(this.id);
    };
    EmployeeDetailsComponent.prototype.loadEmployee = function (id) {
        var _this = this;
        this.employeeService.detail(global_1.Global.BASE_Employee_ENDPOINT, id)
            .subscribe(function (employee) {
            _this.employees = employee;
        }, function (error) { return _this.msg = error; });
    };
    return EmployeeDetailsComponent;
}());
EmployeeDetailsComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: '/app/employees/htmltemplates/app.employee-detail.html'
    }),
    __metadata("design:paramtypes", [employee_service_1.EmployeeService, router_1.ActivatedRoute])
], EmployeeDetailsComponent);
exports.EmployeeDetailsComponent = EmployeeDetailsComponent;
// ====== ./app/Dogs/dog-details.component ======
// Imports
//import { Component, OnInit } from '@angular/core';
//import { EmployeeService } from '../employees/employee.service';
//import { Observable } from 'rxjs/Observable';
//import { ActivatedRoute } from '@angular/router';
//@
//Component({
//    selector: 'my-app',
//    templateUrl: '/app/employees/htmltemplates/app.employee-detail.html',
//    styleUrls: ['app/app.component.css']
//})
////Component class implementing OnInit
//export class EmployeeDetailsComponent implements OnInit {
//    // Private properties for binding
//    private sub: any;
//    private employee: Observable<Object>;
//    constructor(private employeeService: EmployeeService, private route: ActivatedRoute) {
//    }
//    // Load data ones componet is ready
//    ngOnInit() {
//        // Subscribe to route params
//        this.sub = this.route.params.subscribe(params => {
//            let id = params['id'];
//            // Retrieve employee with Id route param
//         this.employee= this.employeeService.findEmployeeById(id);
//        });
//    }
//    ngOnDestroy() {
//        // Clean sub to avoid memory leak
//        this.sub.unsubscribe();
//    }
//} 
//# sourceMappingURL=employee.detail.js.map