"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var employee_service_1 = require("./employee.service");
var forms_1 = require("@angular/forms");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var enum_1 = require("../Shared/enum");
var global_1 = require("../Shared/global");
var router_1 = require("@angular/router");
var EmployeesComponent = (function () {
    function EmployeesComponent(fb, router, employeeService, route) {
        this.fb = fb;
        this.router = router;
        this.employeeService = employeeService;
        this.route = route;
        this.indLoading = false;
    }
    EmployeesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params['id'];
        });
        this.employeeFrm = this.fb.group({
            Id: [''],
            Name: ['', forms_1.Validators.required],
            Gender: ['', forms_1.Validators.required],
            Designation: [''],
            Email: [''],
            Age: [''],
            Phone: ['']
        });
        this.loadEmployees();
    };
    EmployeesComponent.prototype.loadEmployees = function () {
        var _this = this;
        this.indLoading = true;
        this.employeeService.get(global_1.Global.BASE_Employee_ENDPOINT)
            .subscribe(function (employees) {
            _this.employees = employees;
            _this.indLoading = false;
        }, function (error) { return _this.msg = error; });
    };
    EmployeesComponent.prototype.addEmployee = function () {
        this.dbops = enum_1.DBOperation.create;
        this.SetControlsState(true);
        this.modalTitle = "Add New employee";
        this.modalBtnTitle = "Add";
        this.employeeFrm.reset();
        this.modal.open();
    };
    EmployeesComponent.prototype.editEmployee = function (id) {
        this.dbops = enum_1.DBOperation.update;
        this.SetControlsState(true);
        this.modalTitle = "Edit User";
        this.modalBtnTitle = "Update";
        this.employee = this.employees.filter(function (x) { return x.Id === id; })[0];
        this.employeeFrm.setValue(this.employee);
        this.modal.open();
    };
    EmployeesComponent.prototype.deleteEmployee = function (id) {
        this.dbops = enum_1.DBOperation.delete;
        this.SetControlsState(false);
        this.modalTitle = "Confirm to Delete?";
        this.modalBtnTitle = "Delete";
        this.employee = this.employees.filter(function (x) { return x.Id === id; })[0];
        this.employeeFrm.setValue(this.employee);
        this.modal.open();
    };
    EmployeesComponent.prototype.onSubmit = function (formData) {
        var _this = this;
        this.msg = "";
        switch (this.dbops) {
            case enum_1.DBOperation.create:
                this.employeeService.post(global_1.Global.BASE_Employee_ENDPOINT, formData._value).subscribe(function (data) {
                    if (data == 1) {
                        _this.msg = "Data successfully added.";
                        _this.loadEmployees();
                    }
                    else {
                        _this.msg = "There is some issue in saving records, please contact to system administrator!";
                    }
                    _this.modal.dismiss();
                }, function (error) {
                    _this.msg = error;
                });
                break;
            case enum_1.DBOperation.update:
                this.employeeService.put(global_1.Global.BASE_Employee_ENDPOINT, formData._value.Id, formData._value).subscribe(function (data) {
                    if (data == 1) {
                        _this.msg = "Data successfully updated.";
                        _this.loadEmployees();
                    }
                    else {
                        _this.msg = "There is some issue in saving records, please contact to system administrator!";
                    }
                    _this.modal.dismiss();
                }, function (error) {
                    _this.msg = error;
                });
                break;
            case enum_1.DBOperation.delete:
                this.employeeService.delete(global_1.Global.BASE_Employee_ENDPOINT, formData._value.Id).subscribe(function (data) {
                    if (data == 1) {
                        _this.msg = "Data successfully deleted.";
                        _this.loadEmployees();
                    }
                    else {
                        _this.msg = "There is some issue in saving records, please contact to system administrator!";
                    }
                    _this.modal.dismiss();
                }, function (error) {
                    _this.msg = error;
                });
                break;
        }
    };
    EmployeesComponent.prototype.getDetail = function (employee) {
        this.router.navigate(['/employee', employee.Id]);
    };
    EmployeesComponent.prototype.SetControlsState = function (isEnable) {
        isEnable ? this.employeeFrm.enable() : this.employeeFrm.disable();
    };
    return EmployeesComponent;
}());
__decorate([
    core_1.ViewChild('modal'),
    __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
], EmployeesComponent.prototype, "modal", void 0);
EmployeesComponent = __decorate([
    core_1.Component({
        templateUrl: '/app/employees/htmltemplates/app.employees.html'
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder, router_1.Router, employee_service_1.EmployeeService, router_1.ActivatedRoute])
], EmployeesComponent);
exports.EmployeesComponent = EmployeesComponent;
//import { Component, OnInit } from '@angular/core';
//import { EmployeeService } from './employee.service';
//import 'rxjs/add/operator/toPromise';
//import { Observable } from 'rxjs/observable';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
//@Component({
//    selector: 'my-app',
//    templateUrl: '/app/employees/htmltemplates/app.employees.html',
//    styleUrls: ['app/app.component.css']
//})
//export class EmployeesComponent implements OnInit {
//   // employees: Observable<Object[]>;
//    employees: any = {};
//    constructor(private employeeService: EmployeeService) {
//        this.employees =
//         this.employeeService.findEmployees();
//    }
//    ngOnInit() {
//      //  this.employees = this.employeeService.findEmployees();
//    }
//    //ngOnInit() {
//    //    this.employees = ((this.employeeService.findEmployees()) as any);
//    //}
//}
//# sourceMappingURL=employees.component.js.map