"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/do");
require("rxjs/add/operator/catch");
require("rxjs/operator/delay");
require("rxjs/operator/mergeMap");
require("rxjs/operator/switchMap");
var EmployeeService = (function () {
    function EmployeeService(http) {
        this.http = http;
    }
    EmployeeService.prototype.get = function (url) {
        console.log(url + "Employee");
        return this.http.get(url + "Employee")
            .map(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    EmployeeService.prototype.post = function (url, model) {
        var body = JSON.stringify(model);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url + "Post", body, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EmployeeService.prototype.put = function (url, id, model) {
        var body = JSON.stringify(model);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.put(url + "Put" + id, body, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EmployeeService.prototype.delete = function (url, id) {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.delete(url + "Delete" + id, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EmployeeService.prototype.detail = function (url, id) {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        console.log(url + "Detail");
        return this.http.get("http://localhost:58669/api/employeeapi/" + "Detail?id=" + id)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    EmployeeService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    return EmployeeService;
}());
EmployeeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], EmployeeService);
exports.EmployeeService = EmployeeService;
//// Imports
//import { Injectable } from '@angular/core';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
//import { Observable } from 'rxjs/observable'
//import { Http } from '@angular/http';
//import { Jsonp, URLSearchParams } from '@angular/http';
//@Injectable()
//export class EmployeeService { 
//    employees: Observable<Employee[]>;
//    employee:Observable<Employee>;
//    private employeeDetailUrl ='/Home/GetEmployeeDetailById';
//    constructor(private http: Http) { }
//    findEmployees():any{
//        let employeesUrl = '/Home/GetEmploeesSummary';
//          ((this.http
//            .get(employeesUrl).
//            subscribe(response => {
//                let data = response.text() ? response.json() : [{}];
//                if (data) {
//                    this.employees = data;
//                }
//                return JSON.stringify(this.employees);
//            }, error => console.log("did not get any responce!"))));
//        return this.employees;
//    }
//    findEmployeeById(id: number) {
//            let params = new URLSearchParams();
//            params.set('id', (id) as any);
//            params.set('format', 'json');
//            params.set('callback', 'JSONP_CALLBACK');
//            this.http
//                .get(this.employeeDetailUrl, { search: params }).toPromise()
//                .then(response => this.employee= response.json());
//        return this.employee;
//    }
//}
//export class Employee {
//    Name: number;
//    Id: string;
//    Designation: string;
//    Age :number; 
//    Email:string;
//    Phone:string; 
//}
//export class QueryStringParameters {
//    public Id: number = null;
//}
//constructor(private jsonp: Jsonp) { }
//private employeesUrl = '/Home/AjaxCall';
//findEmployees() {
//    return this.jsonp
//        .get(this.employeesUrl)
//        .map(response => <string[]>response.json());
//}
//// get a pet based on their id
//findEmployeeById(id: string) {
//    let params = new URLSearchParams();
//    params.set('id', id);
//    params.set('format', 'json');
//    params.set('callback', 'JSONP_CALLBACK');
//    // Return response
//    return this.jsonp
//        .get(this.employeesUrl, { search: params })
//        .map(response => <string[]>response.json().petfinder.pet);
//}
//} 
//# sourceMappingURL=employee.service.js.map