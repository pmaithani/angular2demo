﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'my-department',
    template: `<h2>Department</h2>
        <input type="text" [value]="department" (input)="departmentInput($event)"/>        
        <p>{{ department }} is also my life</p>`
})
export class DepartmentComponent {
    department = 'IT';
    foodInput(event: Event) {
        const target = event.target as HTMLInputElement;
        this.department = target.value;
    }
}