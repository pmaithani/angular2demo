﻿using System;
using System.Net.Http;
using System.Web.Http;
using Angular2Demo.Models;
using Angular2Demo.Services;

namespace Angular2Demo.Controllers
{
    [RoutePrefix("api/newhomesource")]
    public class HomesController : BaseAPIController
    {
        [Route("homesResult")]
        [HttpGet]
        public HttpResponseMessage GetHomesResults(string marektId, string state)
        {
            return ToJson(HomeSearchDataManager.PerformSearch(Convert.ToInt32(marektId),state));
        }
        [Route("HomeDetail")]
        [HttpGet]
        public HttpResponseMessage HomeDetail(string id)
        {

            return ToJson(ListingDataManager.GetListingDetails(88,id));
        }
        [Route("Login")]
        [HttpGet]
        public HttpResponseMessage Login(string email, string password)
        {
            LoginModel model = LoginModel.GetUser();
            if (email == model.Email && password == model.Password)
            {
                model.Token = "xyz12345";
                model.IsAuthenticated = true;
            }
           
            return ToJson(model);
        }
    }
}
