﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;

using Microsoft.Ajax.Utilities;

namespace Angular2Demo.Controllers
{
    [RoutePrefix("api/employeeapi")]
    public class EmployeeApiController : BaseAPIController
    {
        [Route("Employee")]
        [HttpGet]
        public HttpResponseMessage GetEmpDetail()
        {
       
            return ToJson(AjaxClass.GetEmploees().ToList());
        }

        public HttpResponseMessage Post([FromBody]AjaxClass value)
        {
         
            return ToJson(AjaxClass.SaveEmploees(value));
        }

        public HttpResponseMessage Put(int id, [FromBody]AjaxClass value)
        {
            return ToJson(AjaxClass.SaveEmploees(value));
        }
        public HttpResponseMessage Delete(int id)
        {
          var item=  AjaxClass.GetEmploees().RemoveAll(r => r.Id == id);
            return ToJson(item);
        }
        [Route("Detail")]
        [HttpGet]
        public HttpResponseMessage Detail(string id)
        {
            var item = AjaxClass.GetEmploees().Where(x => x.Id == int.Parse(id));
            return ToJson(item);
        }
    }
}

public class AjaxClass
{
    public string Name { get; set; }
    public string Gender { get; set; }
    public int Id { get; set; }
    public string Designation { get; set; }
    public int Age { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }

    public static List<AjaxClass> GetEmploees()
    {
        List<AjaxClass> users = new List<AjaxClass>
        {
            new AjaxClass
            {
                Id = 1,
                Name = "Pradeep",
                Gender="Male",
                Designation = "Software Engineer",
                Age = 26,
                Email = "pmaithani@ex2india.com",
                Phone = "9650959832"
            },
            new AjaxClass
            {
                Id = 2,
                Name = "Munesh",
                Gender="Male",
        Designation = "Software Engineer",
                Age = 27,
                Email = "mkumar@ex2india.com",
                Phone = "96509459832"
            },
            new AjaxClass
            {
                Id = 3,
                Name = "Rohit",
                Gender="Male",
                Designation = "Software Engineer",
                Age = 26,
                Email = "rarora@ex2india.com",
                Phone = "9830959832"
            },
            new AjaxClass
            {
                Id = 4,
                Name = "Ruchika",
                Gender="Female",
        Designation = "Software QA Engineer",
                Age = 26,
                Email = "rverma@ex2india.com",
                Phone = "9610959832"
            },
            new AjaxClass
            {
                Id = 5,
                Name = "Nidhi",
                Gender="Female",
                Designation = "Product Manager",
                Age = 28,
                Email = "nbhatia@ex2india.com",
                Phone = "9950959832"
            }
        };
        return users;
    }

    public static List<AjaxClass> SaveEmploees(AjaxClass employee)
    {
        GetEmploees().Add(employee);
        return GetEmploees();
    }

}