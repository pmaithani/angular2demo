﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Angular2Demo.Models
{
    [DataContract(Namespace = "http://www.newhomesource.com", Name = "HomeSearchResult")]
    public class HomeSearchResult
    {
        private string _dynamicHomeImageURL = string.Empty;
        private HomeStatus statusType = HomeStatus.NotSet;
        private static CultureInfo ci = new CultureInfo("en-us");
        [DataMember(Name = "Addr")]
        public string StreetAddress { get; set; }
        [DataMember(Name = "City")]
        public string City { get; set; }
        [DataMember(Name = "State")]
        public string State { get; set; }
        [DataMember(Name = "Zip")]
        public string ZipCode { get; set; }
        [DataMember(Name = "County")]
        public string County { get; set; }
        [DataMember(Name = "Br")]
        public int NumberOfBedrooms { get; set; }
        [DataMember(Name = "Ba")]
        public int NumberOfBathrooms { get; set; }
        [DataMember(Name = "HBa")]
        public int HalfBathrooms { get; set; }

        [DataMember(Name = "Gr")]
        public decimal NumberOfGarages { get; set; }
        [DataMember(Name = "Price")]
        public decimal Price { get; set; }
        [DataMember(Name = "Sft")]
        public int Area { get; set; }
        [DataMember(Name = "Status")]
        public String Status { get; set; }
        [DataMember(Name = "HomeId")]
        public int ID { get; set; }
        [DataMember(Name = "CommName")]
        public string CommunityName { get; set; }
        [DataMember(Name = "Brand")]
        public Brand BrandDetails { get; set; }
        [DataMember(Name = "PlanName")]
        public string PlanName { get; set; }
        [DataMember(Name = "IsSpec")]
        public bool IsSpec { get; set; }

        [DataMember(Name = "BuilderId")]
        public int BuilderID { get; set; }

        [DataMember(Name = "MarketId")]
        public int MarketID { get; set; }


        [DataMember(Name = "CommId")]
        public int CommunityID { get; set; }
        [DataMember(Name = "PriceRange")]
        public string PriceRange
        {
            get
            {
                return this.Price.ToString("C0", ci);
            }
            set
            {
            }
        }


        [DataMember(Name = "AreaRange")]
        public string AreaRange
        {
            get
            {
                if (this.Area != 0)
                {
                    return $"{this.Area.ToString("##,###")} sq.ft.";
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
            }
        }

        public string StatusDescription
        {
            get
            {
                return GetStatusName(this.Status);
            }

        }
        public string BathRooms
        {
            get { return ComputeBathrooms(); }
        }

        public HomeStatus StatusType
        {
            get
            {
                if (statusType == HomeStatus.NotSet)
                {
                    statusType = ConvertStatusToStatusType(this.Status);
                }
                return statusType;
            }
            set
            {
                statusType = value;
            }
        }

        public HomeStatus ConvertStatusToStatusType(string listingStatus)
        {
            HomeStatus homeStatusType = HomeStatus.NotSet;
            switch (listingStatus)
            {
                case "A":
                    homeStatusType = HomeStatus.AvailableNow;
                    break;
                case "M":
                    homeStatusType = HomeStatus.ModelHome;
                    break;
                case "Q":
                    homeStatusType = HomeStatus.QuickMoveIn;
                    break;
                case "R":
                    homeStatusType = HomeStatus.ReadyToBuild;
                    break;
                case "UC":
                    homeStatusType = HomeStatus.UnderConstruction;
                    break;
                case "U":
                    homeStatusType = HomeStatus.UnderConstruction;
                    break;
            }
            return homeStatusType;
        }


        public string GetStatusName(string listingStatus)
        {
            string statusName = string.Empty;
            switch (listingStatus)
            {
                case "A":
                    statusName = "Available Now";
                    break;
                case "M":
                    statusName = "Model Home";
                    break;
                case "Q":
                    statusName = "Quick Move-In";
                    break;
                case "R":
                    statusName = "Ready to Build";
                    break;
                case "UC":
                    statusName = "Under Construction";
                    break;
                case "U":
                    statusName = "Under construction";
                    break;
            }
            return statusName;
        }

        public string ComputeBathrooms()
        {
            var baths = NumberOfBathrooms + ((HalfBathrooms > 0) ? 0.5 : 0.0);
            return string.Format(HalfBathrooms < 1 ? "{0:F0}" : "{0:F1}", baths);
        }

        [DataMember(Name = "Thumb1")]
        public string ImageURL
        {
            get
            {
                return _dynamicHomeImageURL;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    String fileName = System.IO.Path.GetFileName(value);
                    int underscoreIndex = fileName.IndexOf("_", StringComparison.Ordinal);
                    if (underscoreIndex > -1)
                    {
                        String fileNameWithoutUnderscore = fileName.Substring(underscoreIndex + 1);
                        value = value.Replace(fileName, fileNameWithoutUnderscore);
                    }
                    value = $"{value}";
                    _dynamicHomeImageURL = value;
                }
                else
                {
                    _dynamicHomeImageURL = "/content/images/no-image.png";
                }
            }
        }
    }
}