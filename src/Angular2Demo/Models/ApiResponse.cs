﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Angular2Demo.Models
{
    public class ApiResponse
    {
        public string Time { get; set; }
        public Error Error { get; set; }
        public string Status { get; set; }
        public ApiResponse()
        {
            this.Error = new Error();
        }
    }
    public enum APIStatus
    {
        OK,
        Fail
    }
}