﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web;

namespace Angular2Demo.Models
{
    public class Brand
    {
        private string _builderName;
        [DataMember(Name = "Id")]
        public int ID { get; set; }
        [DataMember(Name = "Name")]
        public string Name
        {
            get
            {
                return _builderName;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _builderName = Regex.Replace(value, @"(\s|&amp;)", " ");
                }
            }
        }
        [DataMember(Name = "Logo")]
        public string LogoURL { get; set; }
        [DataMember(Name = "LogoSmall")]
        public string SmallLogoURL { get; set; }
        [DataMember(Name = "MarketId")]
        public int MarketID { get; set; }
        [DataMember(Name = "State")]
        public string State { get; set; }
    }
}