﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Angular2Demo.Models
{
    public class ResultCounts
    {

        [DataMember(Name = "CommCount")]
        public int CommunityCount { get; set; }

        [DataMember(Name = "NbyCommCount")]
        public int NearByCommunityCount { get; set; }

        [DataMember(Name = "HotDealsCount")]
        public int HotDealsCount { get; set; }

        [DataMember(Name = "QmiCount")]
        public int QuickMoveInCount { get; set; }

        [DataMember(Name = "HomeCount")]
        public int HomeCount { get; set; }

    }
}