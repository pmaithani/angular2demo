﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Angular2Demo.Models
{
    public class Error
    {
        public string ClassName { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public object InnerException { get; set; }
        public object HelpURL { get; set; }
        public string StackTraceString { get; set; }
        public object RemoteStackTraceString { get; set; }
        public int RemoteStackIndex { get; set; }
        public string ExceptionMethod { get; set; }
        public int HResult { get; set; }
        public string Source { get; set; }
        public object WatsonBuckets { get; set; }

    }
}