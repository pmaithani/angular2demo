﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Angular2Demo.Models
{
    public enum HomeStatus
    {
        AvailableNow = 1,
        UnderConstruction = 2,
        ReadyToBuild = 3,
        ModelHome = 4,
        QuickMoveIn = 5,
        NotSet = 0

    }
}