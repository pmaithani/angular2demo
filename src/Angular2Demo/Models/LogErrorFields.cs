﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Angular2Demo.Models
{
    class LogErrorFields
    {
        public const string FromPage = "fromPage";
        public const string ToPage = "toPage";
        public const string HTTPMethod = "HTTPMethod";
        public const string PassingData = "passingData";
        public const string HostName = "hostname";
        public const string Machine = "machine";
        public const string ErrorDetails = "errorDetails";
        public const string ErrorType = "errorType";
        public const string ProcessID = "processID";
        public const string ProcessName = "processName";
        public const string ThreadName = "threadName";
        public const string Win32ThreadID = "win32ThreadID";
        public const string WindowsIdentity = "windowsIdentity";
        public const string ClientIP = "clientIP";
        public const string ClientPort = "clientPort";
        public const string SessionID = "sessionID";
        public const string Cookies = "cookies";
        public const string UserAgent = "userAgent";
        public const string APIUrl = "apiUrl";
    }
}
