﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Angular2Demo.Models
{
    [DataContract(Namespace = "http://www.newhomesource.com", Name = "HomeSearchResults")]
    public class HomeSearchResults
    {
        public HomeSearchResults()
        {
            Results = new List<HomeSearchResult>();
            SearchResultCounts = new ResultCounts();

        }
        [DataMember(Name = "ResultCounts")]
        public ResultCounts SearchResultCounts { get; set; }
        [DataMember(Name = "Result")]
        public List<HomeSearchResult> Results { get; set; }

     
    }
}
