﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace Angular2Demo.Models
{
    [DataContract(Namespace = "http://www.newhomesource.com", Name = "ListingDetail")]
    public class ListingDetails
    {
        private static CultureInfo ci = new CultureInfo("en-us");
        private string _areaRange;
        private string _priceRange;
        private string _thumbnailPath;

        private string _builderMapURL = string.Empty;
        private List<string> _schoools;




        [DataMember(Name = "Desc")]
        public string Description { get; set; }
        [DataMember(Name = "CommAddr")]
        public string CommunityStreetAddress { get; set; }
        [DataMember(Name = "CommCity")]
        public string CommunityCity { get; set; }
        [DataMember(Name = "CommState")]
        public string CommunityState { get; set; }
        [DataMember(Name = "CommZip")]
        public string CommunityZipCode { get; set; }

        [DataMember(Name = "SpecNum")]
        public string SpecNumber { get; set; }
        [DataMember(Name = "Addr")]
        public string ListingStreetAddress { get; set; }
        [DataMember(Name = "City")]
        public string ListingCity { get; set; }
        [DataMember(Name = "State")]
        public string ListingState { get; set; }
        [DataMember(Name = "Zip")]
        public string ListingZipCode { get; set; }
        [DataMember(Name = "Videos")]


        public string DrivingDirections { get; set; }

        public bool HasDrivingDirections
        {
            get
            {
                return !string.IsNullOrEmpty(this.DrivingDirections);
            }


        }

        [DataMember(Name = "SchoolDistricts")]
        public string SchoolDistricts { get; set; }

        public List<string> Schools
        {
            get
            {
                if (_schoools == null)
                {
                    _schoools = this.SchoolDistricts.Split(new char[] { ',' }).ToList();
                }
                return _schoools;
            }
        }

        [DataMember(Name = "CommLat")]
        public decimal Latitude { get; set; }

        [DataMember(Name = "CommLng")]
        public decimal Longitude { get; set; }


        [DataMember(Name = "PlanName")]
        public string Name { get; set; }

        [DataMember(Name = "PlanId")]
        public int PlanID { get; set; }

        [DataMember(Name = "Map")]
        public string BuilderMapUrl { get; set; }


        [DataMember(Name = "Br")]
        public int Bedrooms { get; set; }
        [DataMember(Name = "Ba")]
        public int Bathrooms { get; set; }

        [DataMember(Name = "HBa")]
        public int HalfBathrooms { get; set; }
        [DataMember(Name = "Gr")]
        public decimal Garage { get; set; }
        [DataMember(Name = "St")]
        public decimal Story { get; set; }
        [DataMember(Name = "Status")]
        public string HomeStatus { get; set; }

        [DataMember(Name = "IsHotHome")]
        public bool IsHotHome { get; set; }
        [DataMember(Name = "HomeId")]
        public int ID { get; set; }
        [DataMember(Name = "Phone")]
        public string PhoneNumber { get; set; }
        [DataMember(Name = "Thumb")]
        public string ThumbnailPath { get; set; }

        [DataMember(Name = "Images")]
      
        public String CommunityOfficeHours { get; set; }

        [DataMember(Name = "PlanViewerUrl")]
        public string FloorPlanViewerUrl { get; set; }
        [DataMember(Name = "TourUrl")]
        public string VirtualTourURL { get; set; }
        public Brand CommunityBrand { get; set; }
        public int BrandID { get; set; }
        public int MarketID { get; set; }
        public string MarketName { get; set; }
        public string CommunityName { get; set; }
        [DataMember(Name = "CommId")]
        public int CommunityID { get; set; }
       
        public string StateName { get; set; }
        public string CommunityDescription { get; set; }
       


        public string NumberOfBathRooms { get { return ComputeBathrooms(); } }

        public string ComputeBathrooms()
        {
            var baths = Bathrooms + ((HalfBathrooms > 0) ? 0.5 : 0.0);
            return string.Format(HalfBathrooms < 1 ? "{0:F0}" : "{0:F1}", baths);
        }
        public string DisplayName
        {
            get
            {
                if (this.IsSpecHome)
                {
                    StringBuilder nameBuilder = new StringBuilder();
                    if (!string.IsNullOrEmpty(this.StreetAddress))
                    {
                        nameBuilder.AppendFormat("{0} - ", this.StreetAddress);
                    }
                    nameBuilder.AppendFormat("{0}, {1} {2} ({3})", this.City, this.State, this.ZipCode, this.Name);
                    return nameBuilder.ToString();
                }
                else
                {
                    return this.Name;
                }


            }
        }

        public string StatusDescription
        {
            get
            {
                return GetStatusName(this.HomeStatus);
            }

        }
        public string GetStatusName(string listingStatus)
        {
            string statusName = string.Empty;
            switch (listingStatus)
            {
                case "A":
                    statusName = "Available Now";
                    break;
                case "M":
                    statusName = "Model Home";
                    break;
                case "Q":
                    statusName = "Quick Move-In";
                    break;
                case "R":
                    statusName = "Ready to Build";
                    break;
                case "UC":
                    statusName = "Under Construction";
                    break;
                case "U":
                    statusName = "Under Construction";
                    break;

            }
            return statusName;
        }

        [DataMember(Name = "Price")]
        public decimal Price { get; set; }
     
        private string GetFormattedPriceRange()
        {
            String formattedPriceRange = string.Empty;

            if (this.Price > 0)
            {
                formattedPriceRange = $"{this.Price.ToString("C0", ci)}";
            }

            return formattedPriceRange;
        }
        [DataMember(Name = "PriceRange")]
        public string PriceRange
        {
            get
            {
                if (_priceRange == null)
                {
                    _priceRange = GetFormattedPriceRange();
                }
                return _priceRange;
            }
            set
            {
            }
        }
        [DataMember(Name = "Sft")]
        public int Area { get; set; }
     
        [DataMember(Name = "AreaRange")]
        public string AreaRange
        {
            get
            {
                if (_areaRange == null)
                {
                    _areaRange = GeFormattedAreaRange();
                }
                return _areaRange;
            }
            set
            {
            }
        }
        private string GeFormattedAreaRange()
        {
            String formattedAreaRange = string.Empty;

            if (this.Area > 0)
            {
                formattedAreaRange = $"{this.Area.ToString("##,###")} sq.ft.";

            }
            return formattedAreaRange;
        }




      

       

        [DataMember(Name = "IsSpec")]
        public bool IsSpecHome
        {
            get
            {
                return !string.IsNullOrEmpty(SpecNumber);
            }
            set
            {

            }
        }
     
        [IgnoreDataMember]
        public bool HasAddress
        {
            get
            {
                return !(string.IsNullOrEmpty(this.StreetAddress) && string.IsNullOrEmpty(this.City) && string.IsNullOrEmpty(this.State) && string.IsNullOrEmpty(this.ZipCode));
            }
        }

        [IgnoreDataMember]
        public bool HasCommunityAddress
        {
            get
            {
                return !(string.IsNullOrEmpty(this.CommunityStreetAddress) && string.IsNullOrEmpty(this.CommunityCity) && string.IsNullOrEmpty(this.CommunityState) && string.IsNullOrEmpty(this.CommunityZipCode));
            }
        }

        public string StreetAddress
        {
            get
            {
                return this.IsSpecHome ? this.ListingStreetAddress : this.CommunityStreetAddress;
            }
        }

        public string City
        {
            get
            {
                return this.IsSpecHome ? this.ListingCity : this.CommunityCity;
            }
        }

        public string State
        {
            get
            {
                return this.IsSpecHome ? this.ListingState : this.CommunityState;
            }
        }

        public string ZipCode
        {
            get
            {
                return this.IsSpecHome ? this.ListingZipCode : this.CommunityZipCode;
            }
        }
        [IgnoreDataMember]
        public bool HasCustomMap
        {
            get
            {
                return !string.IsNullOrEmpty(this.BuilderMapUrl);
            }

        }

        public bool HasVideos { get; set; }



    }
}