﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Angular2Demo.Models
{
    public class APIStatusModel
    {
        public static bool IsApiResponceStatusTrue(ApiResponse apiResponseInstance, string apiUrl)
        {
            bool isStatusOk = false;

            if (apiResponseInstance.Status == APIStatus.OK.ToString())
            {
                isStatusOk = true;
            }
            else
            {
                apiResponseInstance.Error.Data = apiUrl;
                ErrorLogger.LogError(apiResponseInstance.Error);
            }
            return isStatusOk;
        }
    }
}
